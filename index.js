const deepmerge = require('deepmerge');

module.exports = function createMapper(mappers) {
  if (!Array.isArray(mappers)) {
    throw new Error('Array of mappers required');
  }

  return function mapper(x) {
    return mappers.reduce(function (acc, f) {
      return deepmerge(acc, f(x));
    }, {});
  };
};

