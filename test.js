const test = require('tape');
const faker = require('faker');
const createMapper = require('./index.js');

test('empty list of mappers', function (t) {
  const mapper = createMapper([]);

  t.equal(typeof mapper, 'function', 'returns a function');
  t.equal(mapper.length, 1, 'returns a unary function');
  t.deepEqual(mapper({ a: 1 }), {}, 'returns an empty object');

  t.end();
});

test('no mappers', function (t) {
  t.throws(createMapper, new Error('Array of mappers required'), 'throws an error when no mappers are given');

  t.end();
});

test('several mappers given', function (t) {
  function sum(a, b) {
    return a + b;
  }

  function is(transactionType) {
    return function (transaction) {
      return transaction.type === transactionType;
    };
  }

  function getAmount(transaction) {
    return Number(transaction.amount);
  }

  const mapper = createMapper([
      function transactionCount(x) {
        return {
          transactionCount: x.length,
        };
      },

      function getBalance(x) {
        const deposits = x.filter(is('deposit')).map(getAmount).reduce(sum, 0);
        const withdrawals = x.filter(is('withdrawal')).map(getAmount).reduce(sum, 0);
        return {
          balance: deposits - withdrawals
        };
      },

      function totalDeposits(x) {
        return {
          totalDeposits: x.filter(is('deposit')).map(getAmount).reduce(sum, 0)
        };
      },

      function totalWithdrawals(x) {
        return {
          totalWithdrawals: x.filter(is('withdrawal')).map(getAmount).reduce(sum, 0)
        };
      },

      function totalPayments(x) {
        return {
          totalPayments: x.filter(is('payment')).map(getAmount).reduce(sum, 0)
        };
      },

      function totalInvoices(x) {
        return {
          totalInvoices: x.filter(is('invoice')).map(getAmount).reduce(sum, 0)
        };
      }
  ]);

  const mockData = new Array(10000).fill().map(faker.helpers.createTransaction);
  const result = mapper(mockData);

  t.equal(result.transactionCount, 10000);

  t.end();

});

